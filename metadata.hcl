# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

app {
  url = "/"
  author = "HashiCorp"
}

pack {
  name = "hallo_dunia"
  description = "coba deploy nomadpack pake jenkins"
  url = "https://gitlab.com/steven87.ags/hello_world"
  version = "0.0.1"
}
