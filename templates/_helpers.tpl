// allow nomad-pack to set the job name

[[- define "job_name" -]]
[[- if eq .hello_dunia.job_name "" -]]
[[- .nomad_pack.pack.name | quote -]]
[[- else -]]
[[- .hello_dunia.job_name | quote -]]
[[- end -]]
[[- end -]]

// only deploys to a region if specified

[[- define "region" -]]
[[- if not (eq .hello_dunia.region "") -]]
region = [[ .hello_dunia.region | quote]]
[[- end -]]
[[- end -]]
